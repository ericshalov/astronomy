$constellations = array(
        "Ursa Major" => array(
                array("Alpha Ursae Majoris",   "Beta Ursae Majoris"),
                array("Beta Ursae Majoris",    "Gamma Ursae Majoris"),
                array("Gamma Ursae Majoris",   "Delta Ursae Majoris"),
                array("Delta Ursae Majoris",   "Epsilon Ursae Majoris"),
                array("Epsilon Ursae Majoris", "Zeta Ursae Majoris"),
                array("Zeta Ursae Majoris",    "Eta Ursae Majoris"),
                array("Alpha Ursae Majoris",    "Delta Ursae Majoris")
        ),

        "Orion" => array(
                array("Epsilon Orionis","Zeta Orionis"),
                array("Zeta Orionis","Delta Orionis"),
                
                array("Zeta Orionis","Alpha Orionis"),
                array("Zeta Orionis","Kappa Orionis"),
                array("Kappa Orionis","Beta Orionis"),
                array("Delta Orionis","Beta Orionis"),
                array("Delta Orionis","Gamma Orionis"),
                array("Alpha Orionis","Gamma Orionis"),
                array("Alpha Orionis","Zeta Orionis")
        ),
        
        "Casseopeia" => array(
                array("Alpha Cassiopeiae","Beta Cassiopeiae"),
                array("Alpha Cassiopeiae","Gamma Cassiopeiae"),
                array("Gamma Cassiopeiae","Delta Cassiopeiae"),
                array("Delta Cassiopeiae","Epsilon Cassiopeiae")
        ),
        
        "Aries" => array(
                array("Alpha Arietis","Beta Arietis")
        ),
        
        "Pisces" => array(
        ),
        
        "Leo" => array(
                array("Alpha Leonis","Eta Leonis"),
                array("Eta Leonis",  "Phi Leonis"),
                array("Phi Leonis",  "Beta Leonis"),
                array("Beta Leonis", "Delta Leonis"),
                array("Delta Leonis","Phi Leonis"),
                array("Delta Leonis","Gamma Leonis"),
                array("Gamma Leonis","Zeta Leonis"),
                array("Gamma Leonis","Mu Leonis"),
                array("Mu Leonis","Kappa Leonis"),
                array("Kappa Leonis","Lambda Leonis"),
                array("Lambda Leonis","Epsilon Leonis"),
                array("Epsilon Leonis","Mu Leonis"),
                array("Epsilon Leonis","Eta Leonis"),
                array("Phi Leonis","Iota Leonis"),
                array("Iota Leonis","Sigma Leonis")
        ),
        
        "Taurus" => array(
                array("Alpha Tauri","Zeta Tauri"),
              array("Alpha Tauri","Phi Tauri"),
                array("Phi Tauri","Gamma Tauri"),
                array("Gamma Tauri","Lambda Tauri"),
                array("Lambda Tauri","Xi Tauri"),
                array("Xi Tauri","Nu Tauri"),
                array("Gamma Tauri","Delta Tauri"),
                array("Delta Tauri","Epsilon Tauri"),
                array("Epsilon Tauri","Beta Tauri"),
        ),
        
        "Virgo" => array(
                array("Beta Virginis","Gamma Virginis"),
                array("Gamma Virginis","Omicron Virginis"),
                array("Omicron Virginis","Eta Virginis"),
                array("Eta Virginis","Beta Virginis"),
                array("Eta Virginis","Gamma Virginis"),
                array("Gamma Virginis","Delta Virginis"),
                array("Delta Virginis","Epsilon Virginis"),
                array("Gamma Virginis","Theta Virginis"),
                array("Gamma Virginis","Zeta Virginis"),
                array("Theta Virginis","Alpha Virginis"),
                array("Zeta Virginis","Iota Virginis"),
                array("Zeta Virginis","Tau Virginis"),
                array("Iota Virginis","Mu Virginis"),
        ),
);
