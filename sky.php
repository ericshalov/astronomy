<?php
//
//	stars.php
//	PHP (requires libgd)
//
//	Simple polar projection (What you'd see at the north pole, if you
//	look straight up.) of the 300 brightest stars, varying in apparent
//	magnitude from -1.46 down to 3.54.  (The limiting magnitude for
//	typical naked-eye visibility under good conditions is typically
//	around 6.5.)
//
//	(C) Copyright 2017 Eric Shalov.
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//
//	Hipparcos Catalog data, obtained from atlasoftheuniverse.com:
//		http://www.atlasoftheuniverse.com/stars.html
//		https://www.cosmos.esa.int/web/hipparcos
//		http://cdsarc.u-strasbg.fr/viz-bin/Cat?I/311
//
//	Example usage:
//	sky.php?w=600&h=600&labels=2.1&asterisms=1&grid=1&zoom=1.0

// Never cache:
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

$w = $_GET['w'];
$h = $_GET['h'];
$min_label_mag = $_GET['labels'];
$show_asterisms = $_GET['asterisms'];
$draw_grid = $_GET['grid'];
$zoom = $_GET['zoom'];

$debug = 0;

$proj_radius = $w/2 * 1.0;

$asterisms = array(
        "Big Dipper" => array(
                array("Alpha Ursae Majoris",   "Beta Ursae Majoris"),
                array("Beta Ursae Majoris",    "Gamma Ursae Majoris"),
                array("Gamma Ursae Majoris",   "Delta Ursae Majoris"),
                array("Delta Ursae Majoris",   "Epsilon Ursae Majoris"),
                array("Epsilon Ursae Majoris", "Zeta Ursae Majoris"),
                array("Zeta Ursae Majoris",    "Eta Ursae Majoris"),
        ),

        "Casseopeia" => array(
                array("Alpha Cassiopeiae","Beta Cassiopeiae"),
                array("Alpha Cassiopeiae","Gamma Cassiopeiae"),
                array("Gamma Cassiopeiae","Delta Cassiopeiae"),
                array("Delta Cassiopeiae","Epsilon Cassiopeiae")
        ),

        "Orion's Belt" => array(
                array("Epsilon Orionis","Zeta Orionis"),
                array("Epsilon Orionis","Delta Orionis"),
        ),

        "Northern Cross" => array(
                array("Alpha Cygni","Beta Cygni"),
                array("Epsilon Cygni","Delta Cygni"),
        ),

        "Spring Triangle" => array(
                array("Beta Leonis","Alpha Virginis"),
                array("Alpha Boötis","Alpha Virginis"),
                array("Beta Leonis","Alpha Boötis"),
        ),
        
        "Great Square of Pegasus" => array(
                array("Alpha Pegasi","Beta Pegasi"),
                array("Beta Pegasi","Alpha Andromedae"),
                array("Alpha Andromedae","Gamma Pegasi"),
                array("Gamma Pegasi","Alpha Pegasi"),
        ),

        "Summer Triange" => array(
                array("Alpha Cygni","Alpha Aquilae"),
                array("Alpha Aquilae","Alpha Lyrae"),
                array("Alpha Lyrae","Alpha Cygni"),
        ),
        
        "Winter Triangle" => array(
                array("Alpha Canis Majoris","Alpha Orionis"),
                array("Alpha Orionis","Alpha Canis Minoris"),
                array("Alpha Canis Minoris","Alpha Canis Majoris"),
        ),
);

function star_xpos($star) {
        global $debug, $w, $h, $proj_radius;
        
        preg_match('|([0-9]+) ([0-9]+)|', $star['ra'], $s);
        $ra_float = ($s[1] + $s[2]/60) / 24.0 * 360.0;
        $dec_float = ($star['dec']);
        $ra_rad = (90+360.0-$ra_float)/360.0 * 3.14159 * 2.0;
        $dec_rad = ((90-$dec_float) / 360.0) * 3.14159 * 2.0;
        $x = $w/2 + $proj_radius/3 * ($dec_rad * cos($ra_rad));

        return round($x);
}

function star_ypos($star) {
        global $debug, $w, $h, $proj_radius;
        
        preg_match('|([0-9]+) ([0-9]+)|', $star['ra'], $s);
        $ra_float = ($s[1] + $s[2]/60) / 24.0 * 360.0;
        $dec_float = ($star['dec']);
        $ra_rad = (90+360.0-$ra_float)/360.0 * 3.14159 * 2.0;
        $dec_rad = ((90-$dec_float) / 360.0) * 3.14159 * 2.0;
        $y = $h/2 - $proj_radius/3 * ($dec_rad * sin($ra_rad));
        
        return round($y);
}

$datafile = preg_split("/\n/",file_get_contents("stars.dat"));
foreach($datafile as $line) {
        if($line != "")
        $stars[trim(substr($line, 0, 26))] = array(
                'bayer_name' => trim(substr($line, 0, 26)),      // Bayer Name
                'proper_name' => trim(substr($line, 26, 18)),    // Proper Name
                'ra' => substr($line, 44, 6),                    // Right Ascension in hours and minutes for epoch 2000.
                'dec' => substr($line, 50, 7),                   // Declination in degrees for epoch 2000.
                'lon' => substr($line, 57, 6),                   // Galactic longitude
                'lat' => substr($line, 63, 6),                   // Galactic latitude
                'spectral_class' => trim(substr($line, 70,13)),  // Spectral classification of the main stars in the system.
                'app_mag' => substr($line, 83, 5),               // Apparent magnitude
                'app_mag_code' => substr($line, 88, 1),          // Apparent magnitude code: 'v' = varies by > 0.1. 'e' = an eclipsing binary.
                'abs_mag' => substr($line, 90, 5),               // Absolute magnitude
                'abs_mag_code' => substr($line, 95, 1),          // Absolute magnitude code.'v' = magnitude varies by more than 0.1.
                'parallax_milliarcsec' => substr($line, 97, 7),  // The Hipparcos parallax of the star (milli-arcseconds).
                'parallax_error' => substr($line, 104, 7),       // The error in the parallax (milli-arcseconds).
                'dist_ly' => substr($line, 113, 10)              // The distance in light years (=3.2616/parallax).
        );
}

$im     = imagecreate($w, $h);
$black  = imagecolorallocate($im, 0,0,0);
$white  = imagecolorallocate($im, 255,255,255);
$orange = imagecolorallocate($im, 220, 210, 60);
$gray   = imagecolorallocate($im, 127,127,127);
$lightgreen   = imagecolorallocate($im, 127,255,127);
$font = '/usr/share/fonts/truetype/droid/DroidSans-Bold.ttf';

// pre-allocate palette colors for star intensities
for($i = 0; $i < 10; $i++) $grays[$i]   = imagecolorallocate($im, round($i*25.5), round($i*25.5), round($i*25.5));
for($i = 0; $i < 10; $i++) $blues[$i]   = imagecolorallocate($im, round($i*15),round($i*15),round($i*25.5)); // for Class A
for($i = 0; $i < 10; $i++) $oranges[$i] = imagecolorallocate($im, round($i*25.5),round($i*20),round($i*5)); // for Class A

if($draw_grid) {
        // Concentric circles indicating 30', 60', 90' declination
        imagearc($im, $w/2, $h/2, $proj_radius*2*1/3*$zoom, $proj_radius*2*1/3*$zoom, 0,360, $gray);
        imagearc($im, $w/2, $h/2, $proj_radius*2*2/3*$zoom, $proj_radius*2*2/3*$zoom, 0,360, $gray);
        imagearc($im, $w/2, $h/2, $proj_radius*2*3/3*$zoom, $proj_radius*2*3/3*$zoom, 0,360, $gray);

        for($hr = 0; $hr < 24; $hr++) {
                $theta = (0.75 + $hr / 24.0) * 3.14159 * 2.0;
                $rho = $proj_radius;
                imageline($im, $w/2, $h/2, $w/2 + $rho*$zoom * cos($theta), $h/2 + $rho * sin($theta), $gray);
                imagettftext($im, 6, 0, $w/2 + $rho * cos($theta), $h/2 + $rho * 0.97 * sin($theta), $white, $font, $hr);
        }
}

if($debug) {
        header("Content-type: text/plain");
} else {
        header("Content-type: image/png");
}


if($show_asterisms) {
        imagesetthickness($im, 3);
        foreach($asterisms as $name => $constellation) {
                foreach($constellation as $segment) {
                        if($stars[$segment[0]] && $stars[$segment[1]]) {
                                $x1 = star_xpos( $stars[$segment[0]] );
                                $y1 = star_ypos( $stars[$segment[0]] );
                                $x2 = star_xpos( $stars[$segment[1]] );
                                $y2 = star_ypos( $stars[$segment[1]] );

                                imageline($im, $x1, $y1, $x2, $y2, $lightgreen);
                        }
                }
                imagettftext($im, 8, 0, $x2, $y2+10, $lightgreen, $font, $name);
        }
}

foreach($stars as $star) {
        preg_match('|([0-9]+) ([0-9]+)|', $star['ra'], $s);
        $ra_float = ($s[1] + $s[2]/60) / 24.0 * 360.0;
        $dec_float = ($star['dec']);
        $ra_rad = (90+360.0-$ra_float)/360.0 * 3.14159 * 2.0;
        $dec_rad = ((90-$dec_float) / 360.0) * 3.14159 * 2.0;
        $x = $w/2 + $proj_radius/3 * ($dec_rad * cos($ra_rad));
        $y = $h/2 - $proj_radius/3*$zoom * ($dec_rad * sin($ra_rad));
        $star_radius = 3 + ((3.54 - $star['app_mag'])/5.00) * 5.0; // bright star is Sirius: -1.46, dimmest in list is 3.54
        $intensity = (3.54 - $star['app_mag'])/5.00; // apparent magnitude range from brightest to dimmest in top 300 is 5.0
        
        // Select an appropriate color:
        $main_sequence_type = substr($star['spectral_class'],0,1);
        if($main_sequence_type == "A" || $main_sequence_type == "B") $star_color = $blues[round(5+4.0*$intensity)];
        else if($main_sequence_type == "K") $star_color = $oranges[round(5+4.0*$intensity)];
        else $star_color = $grays[round(5+4.0*$intensity)];
        
        imagefilledellipse($im, $x, $y, $star_radius, $star_radius, $star_color);
        
        if($star['app_mag'] < $min_label_mag) imagettftext($im, 6, 0, $x, $y, $star_color, $font, $star['proper_name']);
        
        if($debug) {
                print_r($star);
                echo "ra_float = $ra_float, dec_float = $dec_float, app_mag = ".$star['app_mag'].", star_radius = $star_radius, intensity = $intensity, main_sequence_type = $main_sequence_type\n\n";
        }
}

if(!$debug) imagepng($im);
imagedestroy($im);

?>