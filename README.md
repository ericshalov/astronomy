astronomy
=========
Eric's astronomy code repository.

sky.php
-------
![skyplot1.png](examples/skyplot1.png "skyplot1.png")

Draws a simple polar plot of the night sky.
Example usage:
sky.php?w=600&h=600&labels=2.1&asterisms=1&grid=1&zoom=1.0

Parameters:
- w: width
- h: height
- labels: Minimum magnitude to display a star's name
- asterisms=1: Display popular asterisms
- grid=1: Draw a grid of right-ascension and declination
- zoom=1.0: Zoom the plot, centered on Polaris


License:
--------
The code in this repository is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the {organization} nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
